import { editForm } from 'src/app/formio-edit-form/empty/edit-form.index';
import generalStart from 'src/app/formio-edit-form/pieces/general-start';

let API;
function getArtApi(api) {
  API = api;
}

let builder;
function getBuilder(builderElement) {
    builder = builderElement;
}

function addFormToComponent(data) {
  let clearForm = () => obj => {
    if(obj.components.length > 0) {
      if(obj.components[0].type == 'panel') {
        obj.components.forEach(clearForm());
      } 
      else {
        obj.components = [];
      }
    };
  };
  editForm().components[0].components.forEach(clearForm());
  editForm().components[0].components[0].components = [...generalStart];
  let group = data.reduce((r, a) => {
    r[a.NAME] = [...r[a.NAME] || [], a];
  return r;
  }, {});
  let updateForm = (id, property) => obj => {
      if (obj.IDPROPGROUPTYPE === id) {
          let helpText = (property[0].HELPTEXT ? property[0].HELPTEXT : 'NO HELP TEXT').substring(0, 100);
          let settingsProperty = {
            weight: 0,
            type: 'textfield',
            input: true,
            key: property[0].VARCODE,
            label: property[0].NAME,
            placeholder: property[0].NAME,
            tooltip: property[0].NAME,
            validate: {
              required: true,
            },
          };
          if(property.length > 1) {
            settingsProperty.type = 'select';
            settingsProperty['dataSrc'] = 'values';
            settingsProperty['data'] = {};
            settingsProperty['data']['values'] = [...property];
          }
          obj.components.push(settingsProperty);
      }
      else if (obj.components) {
        obj.components.forEach(updateForm(id, property));
      }
  };
  for(let i in group) {
    editForm().components[0].components.forEach(updateForm(group[i][0].IDPROPGROUPTYPE, group[i]));
  };
  builder.editForm.setForm(editForm());
}

function onEditComponent (component, parent, isNew, isJsonEdit, original) {
    document.getElementById('comp-title').innerHTML = component.type;
    document.getElementById('comp-varname').innerHTML = component.varname;
    if(!component.idtemplatevar) {
        API.sqlService('fb_get_varcontrolprop_newelem',{v_idtemplatevar:component.idvarcontrol, v_idtemplatetype: 1}).subscribe((data) => {
          if(data[0].outBinds) {
            component['artfrontproperties'] = data[0].outBinds.v_varcontrolprop_tt;
            addFormToComponent(data[0].outBinds.v_varcontrolprop_tt);
          }
        }, 
          err => console.log(err)
        );
    } else {
      API.sqlService('fb_get_templatevarprop',{v_idtemplatevar:component.idtemplatevar}).subscribe((data) => {
        component['artfrontproperties'] = data;
        addFormToComponent(data);
      }, 
        err => console.log(err)
      );
    }
}

export const onEdit = {
    getArtApi,
    getBuilder,
    onEditComponent
}