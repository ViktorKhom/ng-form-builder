let builderOptions = {
    builder: {
        basic: false,
        advanced: false,
        data: false,
        layout: false,
        premium: false,
        customBasic: {
          title: 'Basic Components',
          default: true,
          weight: 0,
          components: {
            'input[text]': true,
            'input[number]': true,
            'input[date]': true,
            textArea: {
              title: 'textarea',
              key: 'textArea',
              icon: 'font',
              schema: {
                type: 'textarea',
                varcontrol: 'textarea',
                idvarcontrol: 11,
                key: 'TEXTAREA1',
                varname: 'TEXTAREA1',
                varbind: '',
                label: 'Textarea',
                varlabel: 'Textarea',
                input:true
              }
            },
            'input[checkbox]': true,
            customSelect: {
              title: 'select ',
              key: 'customSelect',
              icon: 'th-list',
              schema: {
                type: 'select',
                varcontrol: 'select',
                idvarcontrol: 1,
                key: 'SELECT1',
                varname: 'SELECT1',
                varbind: '',
                label: 'Select',
                varlabel: 'Select',
                input: true
              }
            },
            inputRadio: {
              title: 'input[radio]',
              key: 'inputRadio',
              icon: 'dot-circle-o',
              schema: {
                type: 'radio',
                varcontrol: 'input[radio]',
                idvarcontrol: 9,
                key: 'RADIO1',
                varname: 'RADIO1',
                varbind: '',
                input: true
              }
            },
            customButton: {
              title: 'button',
              key: 'customButton',
              icon: 'stop',
              schema: {
                type: 'button',
                varcontrol: 'button',
                idvarcontrol: 14,
                key: 'BUTTON1',
                varname: 'BUTTON1',
                varbind: '',
                label: 'Кнопка',
                varlabel: 'Кнопка',
              }
            },
            label: true,
            customGrid: {
              title: 'grid',
              key: 'customGrid',
              icon: 'th',
              schema: {
                type: 'grid',
                varcontrol: 'grid',
                idvarcontrol: 25,
                key: 'GRID1',
                varname: 'GRID1',
                varbind: '',
                label: 'Column',
                varlabel: 'Column'
              }
            },
          }
        },
        customSpecial: {
          title: 'Special Components',
          default: true,
          weight: 0,
          components: {
            template: true,
            'input[email]': true,
            inputHidden: true,
            fastfind: true,
            form: true,
            buttonFile: true,
            component: true,
            span: true,
            inputPassword: true,
            inputDateTime: true,
            buttonDropdown: true,
            alert: true,
            artPhoto: true,
            autoComplete: true,
            bpmnModeler: true,
            bpmnViewer: true,
            buttonGroup: true,
            chart: true,
            codeMirror: true,
            image: true,
            gMaps: true,
            mdProgressLinear: true,
            mdSlider: true,
            qrReader: true,
            switch: true,
            uiModelPanel: true
          }
        },
        customLayout: {
          title: 'Layout Components',
          default: true,
          weight: 0,
          components: {
            fieldSet: {
              title: 'fieldset',
              key: 'fieldSet',
              icon: 'th-large',
              schema: {
                type: 'fieldset',
                varcontrol: 'fieldset',
                idvarcontrol: 32,
                key: 'FIELDSET1',
                varname: 'FIELDSET1',
                label: 'Fieldset',
                varlabel: 'Fieldset',
                varbind: '',
                components: [],
              }
            },
            'list-h-container': true,
            'list-v-container': true,
            'tab': true,
            'row-container': true,
            'treeview': true,
            'grid-container': true,
            'accordion': true,
            'uiCarousel': true
          }
        }
    },
};

export default builderOptions;