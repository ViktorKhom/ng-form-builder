import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ArtapiService } from '../../artapi.service';
import { BehaviorSubject, Observable, EMPTY } from 'rxjs';
import { catchError, shareReplay, tap, map } from 'rxjs/operators';
@Component({
  selector: 'app-form-wrapper',
  templateUrl: './form-wrapper.component.html',
  styleUrls: ['./form-wrapper.component.css']
})
export class FormWrapperComponent implements OnInit {
  private url: string = '/node/mobile';
  private loggedIn = new BehaviorSubject<boolean>(false);
  public user$ = new BehaviorSubject<string>(null);
  public id;
  viewMode = 'tab1';
  public form: any;
  public loading: boolean;
  constructor(private http: HttpClient, private artapi: ArtapiService,
              private _route: ActivatedRoute, private _router: Router) { }

  ngOnInit(): void {
    const urlParams = new URLSearchParams(window.location.search);
    this.id = urlParams.get('idtemplate');
    // window.onbeforeunload = function (e) {
    //   localStorage.setItem('atoken', 'null');
    // };
    const token = localStorage.getItem("atoken");
    if(!token || token == 'null') {
        this.auth();
    } else {
        this.getFormData();
    }
  }

  saveForm() {
    this.loading = true;
    let id = this.id ? this.id : 21618;
    this.artapi.sqlService('fb_save_templatevar',{p_idtemplate: this.id, p_js: JSON.stringify(this.form), p_idbreakpoint_arr: [0,1,2,3,4]}).subscribe((data) => {
      this.loading = false;
      console.log(data);
    }, 
      err => console.log(err)
    );
  }

  getFormData() {
      let id = this.id ? this.id : 21618;
      this.artapi.sqlService('fb_get_template_json_n',{p_idtemplate: id, p_filledmark: 0}).subscribe((data) => {
        if (data.length > 0 && data[0].outBinds && data[0].outBinds.result != null) {
          this.form = JSON.parse(data[0].outBinds.result);
        }
        if(!this.form) {
          this.getFormDataLocally();
        }
      }, 
        (err) => {
          console.log(err);
          this.getFormDataLocally();
        }
      );
  }

  getFormDataLocally() {
    this.http.get('/assets/data/rootForm.json').subscribe((formData: any) => {
      this.form = formData;
    });
  }

  auth() {
    return this.login('khomenko', 'vkhom_baza10').subscribe(() => {
     console.log('User Logged In');
     this.getFormData();
    });
  }

  login(login: string, password: string) {
    return this.http.post<any>(this.url + '/auth', {login, pass: password}, {observe: 'response'}).pipe(
      catchError(error => {
        this.getFormDataLocally();
        console.error(error);
        return EMPTY;
      }),
      shareReplay(),
      tap(res => {
        this.setSession(res);
        this.loggedIn.next(true);
        this.user$.next(login);
      })
    )
  }

  private setSession(res) {
    console.log('--setSession', res);
    localStorage.setItem('atoken', res.headers.get('Authorization'));
  }

  onFormEvent(event) {
  }

}
