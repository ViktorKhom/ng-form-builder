import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-form-json',
  templateUrl: './form-json.component.html',
  styleUrls: ['./form-json.component.css']
})
export class FormJsonComponent implements OnInit {
  @ViewChild('json') jsonElement?: ElementRef;
  @Input() form: Object;

  constructor() { }

  ngOnInit(): void {
  }

}
