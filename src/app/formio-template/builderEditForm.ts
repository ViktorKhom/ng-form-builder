
let builderEditForm = `<div class="card formio-edit-form no-radius" ref="dragComponent">
                          <div class="card-header no-radius formio-edit-form__head">
                              <h2 class="formio-edit-form__h" >
                                  # <span id="comp-title"></span>  <br />
                                    <span id="comp-varname"></span>  <br />
                              </h2>
                              <div class="formio-edit-form__btn">
                                <button type="button" class="layout-margin btn btn-primary mr-2 copyComponentBtnForm">
                                    <i class="fa m-0 fa-clone" aria-label="Копировать"></i>
                                </button>
                                <button type="button" class="layout-margin btn btn-primary mr-2 removeComponentBtnForm">
                                    <i class="fa m-0 fa-trash-o" aria-label="Удалить"></i>
                                </button>
                                <div class="saveComponentBtn" ref="saveButton"></div>
                              </div>
                          </div>
                          <div class="formio-edit-form__accord">
                              <div ref="editForm">
                                  {{ctx.editForm}}
                              </div>
                          </div>
                        </div>`;
export default builderEditForm;

