let builderComponent = `<div class="builder-component"  ref="dragComponent">
                                <div class="component-btn-group" data-noattach="true">
                                    <div class="btn btn-xxs btn-secondary component-settings-button component-settings-button-remove" ref="removeComponent">
                                        <i class="{{ctx.iconClass('remove')}}"></i>
                                    </div>
                                    <div class="btn btn-xxs btn-secondary component-settings-button component-settings-button-edit" ref="editComponent">
                                        <i class="{{ctx.iconClass('edit')}}"></i>
                                    </div>
                                </div>
                                <div class="builder-component__edit" ref="showTitle">
                                    {{ctx.html}}
                                </div>
                        </div>`;
export default builderComponent;