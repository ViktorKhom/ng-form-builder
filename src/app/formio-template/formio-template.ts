import builder from './builder';
import builderComponent from './builderComponent';
import builderEditForm from './builderEditForm';
import builderSidebar from './builderSidebar';
import builderTab from './builderTab';

let CurrentTemplate = {
    builder: {
        form: builder
    },
    builderSidebar: {
        form: builderSidebar
    },
    builderEditForm: {
        form: builderEditForm
    },
    builderComponent: {
        form: builderComponent
    },
    tab: {
        form: builderTab
    },
    labelElement: {
        form: `<label class="{{border ? 'border-label' : ''}}"> {{ctx.labelTitle}} </label>`
    }
};

export {CurrentTemplate};