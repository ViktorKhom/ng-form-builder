let builderTab = `<div id="accordion" class="accordion">
                            {% ctx.component.components.forEach(function(tab, index) { %}
                                <div class="card form-builder-panel">
                                    <div class="card-header no-radius form-builder-group-header">
                                    <h2 class="mb-0 mt-0">
                                        <button class="btn btn-block builder-group-button" data-toggle="collapse" data-target="#{{ tab.key == null ? '' : tab.key}}" aria-controls="{{ tab.key == null ? '' : tab.key}}" aria-expanded="false" ref="{{ tab.tabLinkKey == null ? '' : tab.tabLinkKey}}">
                                        {{ctx.t(tab.label)}}
                                        </button>
                                    </h2>
                                    </div>
                                    <div id="{{ tab.key == null ? '' : tab.key}}"
                                        ref="{{ ctx.tabKey == null ? '' : ctx.tabKey}}" class="collapse card-body" data-parent="#accordion">
                                        {{ ctx.tabComponents[index] }}
                                    </div>
                                </div>
                            {% }) %}
                        </div>`;
export default builderTab;