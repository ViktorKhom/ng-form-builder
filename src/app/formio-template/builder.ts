let builder = `<div class="formio builder row formbuilder" id="formio-builder">
                        <div class="col-xs-3 col-sm-3 col-md-3 formcomponents" id="formcomponents">
                            {{ctx.sidebar}}
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-9 formarea" ref="form">
                            {{ctx.form}}
                        </div>
                  </div>`;
export default builder;