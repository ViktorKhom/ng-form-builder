import editFormAdditional  from './form-edit-additional';
import editFormGeneral  from './form-edit-general';
import editFormPositioning  from './form-edit-positioning';
import editFormStyles  from './form-edit-styles';
import editFormValidation  from './form-edit-validation';

import * as _lodash from 'lodash';

let components = _lodash.default.cloneDeep([{
    type: 'tabs',
    key: 'tabs',
    components: [{
      label: 'Общие',
      key: 'general',
      weight: 0,
      IDPROPGROUPTYPE: 1,
      components: editFormGeneral
    }, {
      label: 'Позиционирование',
      key: 'positioning',
      weight: 10,
      IDPROPGROUPTYPE: 2,
      components: editFormPositioning
    },
    {
      label: 'Дополнительные',
      key: 'additional',
      weight: 10,
      components: editFormAdditional
    },
    {
      label: 'Стили',
      key: 'styles',
      weight: 10,
      components: editFormStyles
    },
    {
      label: 'Валидация',
      key: 'validation',
      weight: 10,
      components: editFormValidation
    }]
  }]);

export function editForm() {
  return {
    components: components
  }
}