const editFormAdditional = [
    {
        type: 'panel',
        title: 'Формат данных',
        theme: 'default',
        IDPROPGROUPTYPE:9,
        components: []
    },
    {
        type: 'panel',
        title: 'Tooltip',
        theme: 'default',
        IDPROPGROUPTYPE:10,
        components: []
    },
    {
        type: 'panel',
        title: 'Angular директивы',
        theme: 'default',
        IDPROPGROUPTYPE:33,
        components: []
    },
    {
        type: 'panel',
        title: 'Selection (только для grid)',
        theme: 'default',
        components: []
    }
  ];
  
  
  export default editFormAdditional;