import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS  } from '@angular/common/http';

import { FormioModule } from 'angular-formio';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTabsModule } from '@angular/material/tabs'; 
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { Templates } from 'angular-formio';

import { AuthInterceptor } from './auth.interceptor';

import { AppComponent } from './app.component';
import { CustomFormBuilderComponent } from './components/custom-form-builder/custom-form-builder.component';
import { FormJsonComponent } from './components/form-json/form-json.component';
import { FormWrapperComponent } from './components/form-wrapper/form-wrapper.component';
import { RenderedFormComponent } from './components/rendered-form/rendered-form.component';

import { AlertComponent } from './formio-components/alert/alert.component';
import { ArtPhotoComponent } from './formio-components/art-photo/art-photo.component';
import { AutoCompleteComponent } from './formio-components/auto-complete/auto-complete.component';
import { BpmnModelerComponent } from './formio-components/bpmn-modeler/bpmn-modeler.component';
import { BpmnViewerComponent } from './formio-components/bpmn-viewer/bpmn-viewer.component';
import { ButtonDropdownComponent } from './formio-components/button-dropdown/button-dropdown.component';
import { ButtonFileComponent } from './formio-components/button-file/button-file.component';
import { ButtonGroupComponent } from './formio-components/button-group/button-group.component';
import { ChartComponent } from './formio-components/chart/chart.component';
import { CodeMirrorComponent } from './formio-components/code-mirror/code-mirror.component';
import { ComponentComponent } from './formio-components/component/component.component';
import { CurrentTemplate } from './formio-template/formio-template';
import { FastFindComponent } from './formio-components/fast-find/fast-find.component';
import { FormComponent } from './formio-components/form/form.component';
import { GMapsComponent } from './formio-components/g-maps/g-maps.component';
import { ImageComponent } from './formio-components/image/image.component';
import { InputDateTimeComponent } from './formio-components/input-date-time/input-date-time.component';
import { InputEmailComponent } from './formio-components/input-email/input-email.component';
import { InputHiddenComponent } from './formio-components/input-hidden/input-hidden.component';
import { InputPasswordComponent } from './formio-components/input-password/input-password.component';
import { LabelComponent } from './formio-components/label/label.component';
import { MdProgressLinearComponent } from './formio-components/md-progress-linear/md-progress-linear.component';
import { MdSliderComponent } from './formio-components/md-slider/md-slider.component';
import { QrReaderComponent } from './formio-components/qr-reader/qr-reader.component';
import { SpanComponent } from './formio-components/span/span.component';
import { SwitchComponent } from './formio-components/switch/switch.component';
import { TemplateComponent } from './formio-components/template/template.component';
import { UiCarouselComponent } from './formio-components/ui-carousel/ui-carousel.component';
import { UiModelPanelComponent } from './formio-components/ui-model-panel/ui-model-panel.component';
import { VideoComponent } from './formio-components/video/video.component';
import { registerAlertComponent } from './formio-components/alert/alert.formio';
import { registerArtPhotoComponent } from './formio-components/art-photo/art-photo.formio';
import { registerAutoCompleteComponent } from './formio-components/auto-complete/auto-complete.formio';
import { registerBpmnModelerComponent } from './formio-components/bpmn-modeler/bpmn-modeler.formio';
import { registerBpmnViewerComponent } from './formio-components/bpmn-viewer/bpmn-viewer.formio';
import { registerButtonDropdownComponent } from './formio-components/button-dropdown/button-dropdown.formio';
import { registerButtonFileComponent } from './formio-components/button-file/button-file.formio';
import { registerButtonGroupComponent } from './formio-components/button-group/button-group.formio';
import { registerChartComponent } from './formio-components/chart/chart.formio';
import { registerCodeMirrorComponent } from './formio-components/code-mirror/code-mirror.formio';
import { registerComponentComponent } from './formio-components/component/component.formio';
import { registerFastFindComponent } from './formio-components/fast-find/fast-find.formio';
import { registerFormComponent } from './formio-components/form/form.formio';
import { registerGMapsComponent } from './formio-components/g-maps/g-maps.formio';
import { registerImageComponent } from './formio-components/image/image.formio';
import { registerInputDateTimeComponent } from './formio-components/input-date-time/input-date-time.formio';
import { registerInputEmailComponent } from './formio-components/input-email/input-email.formio';
import { registerInputHiddenComponent } from './formio-components/input-hidden/input-hidden.formio';
import { registerInputPasswordComponent } from './formio-components/input-password/input-password.formio';
import { registerLabelComponent } from './formio-components/label/label.formio';
import { registerMdProgressLinearComponent } from './formio-components/md-progress-linear/md-progress-linear.formio';
import { registerMdSliderComponent } from './formio-components/md-slider/md-slider.formio';
import { registerQrReaderComponent } from './formio-components/qr-reader/qr-reader.formio';
import { registerSpanComponent } from './formio-components/span/span.formio';
import { registerSwitchComponent } from './formio-components/switch/switch.formio';
import { registerTemplateComponent } from './formio-components/template/template.formio';
import { registerUiFormioComponent } from './formio-components/ui-carousel/ui-carousel.formio';
import { registerUiModelPanelComponent } from './formio-components/ui-model-panel/ui-model-panel.formio';
import { registerVideoComponent } from './formio-components/video/video.formio';
import { registerInputTextComponent } from './formio-components/input-text/input-text.formio';
import { registerInputSelectComponent } from './formio-components/input-select/input-select.formio';
import { registerInputDateComponent } from './formio-components/input-date/input-date.formio';
import { registerInputCheckboxComponent } from './formio-components/input-checkbox/input-checkbox.formio';
import { registerInputNumberComponent } from './formio-components/input-number/input-number.formio';
import { InputTextComponent } from './formio-components/input-text/input-text.component';
import { InputDateComponent } from './formio-components/input-date/input-date.component';
import { InputCheckboxComponent } from './formio-components/input-checkbox/input-checkbox.component';
import { InputSelectComponent } from './formio-components/input-select/input-select.component';
import { ButtonComponent } from './formio-components/button/button.component';
import { InputNumberComponent } from './formio-components/input-number/input-number.component';
import { InputRadioComponent } from './formio-components/input-radio/input-radio.component';

import { Components } from 'formiojs';
import RowContainer  from './formio-components/row-container/RowContainer';
import LabelElementComponent  from './formio-components/label/Label';
import TreeViewComponent  from './formio-components/treeview/TreeView';
import TabComponent  from './formio-components/tab/Tab';
import ListVContainerComponent  from './formio-components/list-v-container/ListVContainer';
import ListHContainerComponent  from './formio-components/list-h-container/ListHContainer';
import GridContainer  from './formio-components/grid-container/GridContainer';
import Accordion  from './formio-components/accordion/Accordion';

Components.addComponent('row-container', RowContainer);
Components.addComponent('label', LabelElementComponent);
Components.addComponent('treeview', TreeViewComponent);
Components.addComponent('tab', TabComponent);
Components.addComponent('list-v-container', ListVContainerComponent);
Components.addComponent('list-h-container', ListHContainerComponent);
Components.addComponent('grid-container', GridContainer);
Components.addComponent('accordion', Accordion);
Templates.current = CurrentTemplate;

@NgModule({
  declarations: [
    AlertComponent,
    AppComponent,
    ArtPhotoComponent,
    AutoCompleteComponent,
    BpmnModelerComponent,
    BpmnViewerComponent,
    ButtonDropdownComponent,
    ButtonFileComponent,
    ButtonGroupComponent,
    ChartComponent,
    CodeMirrorComponent,
    ComponentComponent,
    CustomFormBuilderComponent,
    FastFindComponent,
    FormComponent,
    FormJsonComponent,
    FormWrapperComponent,
    GMapsComponent,
    ImageComponent,
    InputDateTimeComponent,
    InputEmailComponent,
    InputHiddenComponent,
    InputPasswordComponent,
    LabelComponent,
    MdProgressLinearComponent,
    MdSliderComponent,
    QrReaderComponent,
    RenderedFormComponent,
    SpanComponent,
    SwitchComponent,
    TemplateComponent,
    UiCarouselComponent,
    UiModelPanelComponent,
    VideoComponent,
    InputTextComponent,
    InputDateComponent,
    InputCheckboxComponent,
    InputSelectComponent,
    ButtonComponent,
    InputNumberComponent,
    InputRadioComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormioModule,
    HttpClientModule,
    MatExpansionModule,
    MatTabsModule,
    NgxJsonViewerModule,
    FormsModule,
    RouterModule.forRoot([])
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    registerAlertComponent(injector);
    registerArtPhotoComponent(injector);
    registerAutoCompleteComponent(injector);
    registerBpmnModelerComponent(injector);
    registerBpmnViewerComponent(injector);
    registerButtonDropdownComponent(injector);
    registerButtonFileComponent(injector);
    registerButtonGroupComponent(injector);
    registerChartComponent(injector);
    registerCodeMirrorComponent(injector);
    registerComponentComponent(injector);
    registerFastFindComponent(injector);
    registerFormComponent(injector);
    registerGMapsComponent(injector);
    registerImageComponent(injector);
    registerInputDateTimeComponent(injector);
    registerInputEmailComponent(injector);
    registerInputHiddenComponent(injector);
    registerInputPasswordComponent(injector);
    // registerLabelComponent(injector);
    registerMdProgressLinearComponent(injector);
    registerMdSliderComponent(injector);
    registerQrReaderComponent(injector);
    registerSpanComponent(injector);
    registerSwitchComponent(injector);
    registerTemplateComponent(injector);
    registerUiFormioComponent(injector);
    registerUiModelPanelComponent(injector);
    registerVideoComponent(injector);
    registerInputTextComponent(injector);
    registerInputSelectComponent(injector);
    registerInputDateComponent(injector);
    registerInputCheckboxComponent(injector);
    registerInputNumberComponent(injector);
  }
 }
