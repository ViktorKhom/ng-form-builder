import { Injector } from '@angular/core';
import { QrReaderComponent } from './qr-reader.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'qrReader',
  selector: 'formio-qr-reader',
  title: 'qr-reader',
  group: 'special',
  icon: 'qrcode',
  editForm: editForm,
  schema: {
    type: 'qr-reader',
    varcontrol: 'qr-reader',
    idvarcontrol: 51,
    key: 'QR-READER1',
    varname: 'QR-READER1',
    varbind: '',
    label: 'Qr-reader',
    varlabel: 'Qr-reader',
  }
};

export function registerQrReaderComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, QrReaderComponent, injector);
}