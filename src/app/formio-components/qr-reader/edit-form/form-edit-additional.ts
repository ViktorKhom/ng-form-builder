const editFormAdditional = [
    {
        type: 'panel',
        title: 'Pagination (только для grid)',
        theme: 'default',
        components: [
            {
                weight: 0,
                type: 'select',
                input: true,
                key: 'data-smart-pagination',
                label: 'data-smart-pagination',
                placeholder: '',
                tooltip: 'Метка пагинации  (только для таблиц и списков) По умолчанию без пагинации.',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'Установить пагинацию (Установить пагинацию)',
                          value: 'data-smart-pagination'
                      }
                  ]
                },
              },
              {
                  weight: 20,
                  type: 'textfield',
                  input: true,
                  key: 'data-pagination-page-size',
                  label: 'data-pagination-page-size',
                  placeholder: '',
                  tooltip: '(Default: 10) - Число элементов на странице при пагинации',
              },
              {
                  weight: 20,
                  type: 'textfield',
                  input: true,
                  key: 'smart-selection',
                  label: 'smart-selection',
                  placeholder: '',
                  tooltip: 'Контрол формы типа pagination, используемый для пагинации. По умолчанию PAGINATION_1',
              },
        ]
    },
    {
        type: 'panel',
        title: 'Selection (только для grid)',
        theme: 'default',
        components: [
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'smart-selection',
                label: 'smart-selection',
                placeholder: '',
                tooltip: 'ВЫбор способа выделения строк в grid',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'selection-all (Применение checkbox&keys)',
                          value: 'all'
                      },
                      {
                        label: 'selection-checkbox (Применение checkbox)',
                        value: 'checkbox'
                      },
                      {
                        label: 'selection-keys (Применение keys)',
                        value: 'keys'
                      },
                      {
                        label: 'selection-none (Отсутствие выделения)',
                        value: 'none'
                      },
                  ]
                },
              },
              {
                weight: 20,
                type: 'select',
                input: true,
                key: 'multi-select',
                label: 'multi-select',
                placeholder: '',
                tooltip: 'Включение мулти выбора строк в grid',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'false-value (значение=false)',
                          value: '0'
                      },
                      {
                        label: 'true-value (значение=true)',
                        value: '1'
                      }
                  ]
                },
              },
        ]
    },
    {
        type: 'panel',
        title: 'Angular директивы',
        theme: 'default',
        components: [
            {
                weight: 20,
                type: 'textfield',
                input: true,
                key: 'ng-show',
                label: 'ng-show',
                placeholder: '',
                tooltip: 'Свойство отображает данные при выполнении заданного условия'
            },
            {
                weight: 20,
                type: 'textfield',
                input: true,
                key: 'ng-hide',
                label: 'ng-hide',
                placeholder: '',
                tooltip: 'Свойство скрывает данные при выполнении заданного условия'
            },
            {
                weight: 20,
                type: 'textfield',
                input: true,
                key: 'ngIf',
                label: 'ngIf',
                placeholder: '',
                tooltip: 'Свойство удаляет или воссоздает на экран элемент, для которого задано выражение. Если выражение, присвоенное ngIf, оценивается как ложное значение, тогда элемент удаляется из контейнера, иначе клон элемента снова вводится в контейнер. ng-If отличается от ng-Show и ng-Hide тем, что ngIf полностью удаляет и воссоздает элемент в контейнере, а не меняет его видимость . Обратите внимание, что при удалении элемента с помощью ngIf его область уничтожается и создается новая область, когда элемент восстанавливается.'
            },
            {
                weight: 20,
                type: 'textfield',
                input: true,
                key: 'ngClass',
                label: 'ngClass',
                placeholder: '',
                tooltip: 'Директива, которая позволяет использовать класс в зависимости от заданных условий. { "наименование класса 1" : условие 1, "наименование класса 2": условие 2} пример: {"art-txt-color-lgrey" : ctrl.info.idfield == 0}'
            },
            {
                weight: 20,
                type: 'textfield',
                input: true,
                key: 'ng-parent-class',
                label: 'ng-parent-class',
                placeholder: '',
                tooltip: 'Директива, которая позволяет использовать класс в зависимости от заданных условий. Применяется к родительскому section'
            },
        ]
    },
  ];
  
  
  export default editFormAdditional;