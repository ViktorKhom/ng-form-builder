import { Injector } from '@angular/core';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import { GMapsComponent } from './g-maps.component';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'gMaps',
  selector: 'formio-g-maps',
  title: 'g-maps',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'g-maps',
    varcontrol: 'g-maps',
    idvarcontrol: 52,
    key: 'G-MAPS1',
    varname: 'G-MAPS1',
    varbind: '',
    label: 'G-maps',
    varlabel: 'G-maps',
  }
};

export function registerGMapsComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, GMapsComponent, injector);
}