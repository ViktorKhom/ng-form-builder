import { Injector } from '@angular/core';
import { SwitchComponent } from './switch.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'switch',
  selector: 'formio-switch',
  title: 'switch',
  group: 'special',
  icon: 'toggle-on',
  editForm: editForm,
  schema: {
    type: 'switch',
    varcontrol: 'switch',
    idvarcontrol: 44,
    key: 'SWITCH1',
    varname: 'SWITCH1',
    varbind: '',
    label: 'Switch',
    varlabel: 'Switch',
  }
};

export function registerSwitchComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, SwitchComponent, injector);
}