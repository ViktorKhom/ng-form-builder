import { Injector } from '@angular/core';
import { InputCheckboxComponent } from './input-checkbox.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'input[checkbox]',
  selector: 'formio-input-checkbox',
  title: 'input[checkbox]',
  group: 'custom',
  icon: 'th-list',
  editForm: editForm,
  schema: {
    type: 'input[checkbox]',
    varcontrol: 'input[checkbox]',
    idvarcontrol: 3,
    key: 'CHECKBOX1',			  	
    varname: 'CHECKBOX1',
    varbind: '',
    label: 'Checkbox',
    varlabel: 'Checkbox',
    input: true,
  }
};

export function registerInputCheckboxComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, InputCheckboxComponent, injector);
}