import { Injector } from '@angular/core';
import { ButtonGroupComponent } from './button-group.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'buttonGroup',
  selector: 'formio-button-group',
  title: 'button[group]',
  group: 'special',
  icon: 'stop',
  editForm: editForm,
  schema: {
    type: 'buttongroup',
    varcontrol: 'buttongroup',
    idvarcontrol: 28,
    key: 'BUTTONGROUP1',
    varname: 'BUTTONGROUP1',
    varbind: '',
    label: 'Buttongroup',
    varlabel: 'Buttongroup',
  }
};

export function registerButtonGroupComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, ButtonGroupComponent, injector);
}