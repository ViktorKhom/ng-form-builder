import { Components } from 'formiojs';
const NestedComponent = (Components as any).components.nested;
import * as _tooltip from "tooltip.js";
import  { editForm }  from './edit-form/edit-form.index';

export default class TabComponent extends NestedComponent {
  static schema(...extend) {
    return NestedComponent.schema({
      type: 'tab',
      varcontrol: 'tab',
      idvarcontrol: 7,
      key: 'TAB1',
      varname: 'TAB1',
      varlabel: 'Tab1',
      varbind: '',
      input: false,
      persistent: false,
      components: []	
    }, ...extend);
  }

  static get builderInfo() {
    return {
      title: 'tab',
      group: 'layout',
      icon: 'list-alt',
      weight: 70,
      schema: TabComponent.schema()
    };
  }

  get defaultSchema() {
    return TabComponent.schema();
  }

  get className() {
    return `form-group ${super.className} tab`;
  }

  get templateName() {
    return 'fieldset';
  }

  attach(element) {
    const refs = {'showTitle': ''};
    this.loadRefs(element, refs);
    this.tooltip = new _tooltip.default(element, {
      trigger: 'hover click',
      placement: 'top',
      html: true,
      title: this.t(this.component.key)
    });
    return super.attach(element);
  }


  constructor(component, options, data) {
    super(component, options, data);
    this.noField = true;
    this.component = component;
  }
}

TabComponent.editForm = editForm;