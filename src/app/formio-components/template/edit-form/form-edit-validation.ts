const editFormValidation = [
    {
        weight: 10,
        type: 'checkbox',
        label: 'Включить валидацию',
        key: 'validationmark',
        input: true
    }
];

export default editFormValidation;