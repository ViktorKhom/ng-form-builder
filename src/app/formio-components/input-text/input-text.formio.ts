import { Injector } from '@angular/core';
import { InputTextComponent } from './input-text.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'input[text]',
  selector: 'formio-input-text',
  title: 'input[text]',
  group: 'custom',
  icon: 'terminal',
  editForm: editForm,
  schema: {
    type: 'input[text]',
    varcontrol: 'input[text]',
    idvarcontrol: 2,
    key: 'TEXT1',
    varname: 'TEXT1',
    varbind: '',
    label: 'Text',
    varlabel: 'Text',
    input: true,
  }
};

export function registerInputTextComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, InputTextComponent, injector);
}