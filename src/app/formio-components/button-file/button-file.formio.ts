import { Injector } from '@angular/core';
import { ButtonFileComponent } from './button-file.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'buttonFile',
  selector: 'formio-button-file',
  title: 'button[file]',
  group: 'special',
  icon: 'stop',
  editForm: editForm,
  schema: {
    type: 'button[file]',
    varcontrol: 'button[file]',
    idvarcontrol: 8,
    key: 'BUTTONFILE1',			  	
    varname: 'BUTTONFILE1',
    label: 'Кнопка',
    varlabel: 'Кнопка',
  }
};

export function registerButtonFileComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, ButtonFileComponent, injector);
}