import { Injector } from '@angular/core';
import { AlertComponent } from './alert.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'alert',
  selector: 'formio-alert',
  title: 'alert',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'alert',
    varcontrol: 'alert',
    idvarcontrol: 27,
    key: 'ALERT1',
    varname: 'ALERT1',
    varbind: '',
    label: 'Alert',
    varlabel: 'Alert',
  }
};

export function registerAlertComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, AlertComponent, injector);
}