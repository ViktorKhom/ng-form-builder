import { Injector } from '@angular/core';
import { BpmnModelerComponent } from './bpmn-modeler.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'bpmnModeler',
  selector: 'formio-bpmn-modeler',
  title: 'bpmn-modeler',
  group: 'special',
  icon: 'file-code-o',
  editForm: editForm,
  schema: {
    type: 'bpmn-modeler',
    varcontrol: 'bpmn-modeler',
    idvarcontrol: 48,
    key: 'BPMN-MODELER1',
    varname: 'BPMN-MODELER1',
    varbind: '',
    label: 'Bpmn-modeler',
    varlabel: 'Bpmn-modeler',
  }
};

export function registerBpmnModelerComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, BpmnModelerComponent, injector);
}