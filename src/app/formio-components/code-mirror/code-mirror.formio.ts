import { Injector } from '@angular/core';
import { CodeMirrorComponent } from './code-mirror.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'codeMirror',
  selector: 'formio-code-mirror',
  title: 'code-mirror',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'code-mirror',
    varcontrol: 'code-mirror',
    idvarcontrol: 50,
    key: 'CODE-MIRROR1',
    varname: 'CODE-MIRROR1',
    varbind: '',
    label: 'Code-mirror',
    varlabel: 'Code-mirror',
  }
};

export function registerCodeMirrorComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, CodeMirrorComponent, injector);
}