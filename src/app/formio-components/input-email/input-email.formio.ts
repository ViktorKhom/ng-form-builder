import { Injector } from '@angular/core';
import { InputEmailComponent } from './input-email.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'input[email]',
  selector: 'formio-input-email',
  title: 'input[email]',
  group: 'special',
  icon: 'at',
  editForm: editForm,
  schema: {
    type: 'input[email]',
    varcontrol: 'input[email]',
    idvarcontrol: 10,
    key: 'EMAIL1',
    varname: 'EMAIL1',
    varbind: '',
    label: 'Email',
    varlabel: 'Email',
    input: true,
  },
};

export function registerInputEmailComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, InputEmailComponent, injector);
}