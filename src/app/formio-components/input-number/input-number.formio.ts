import { Injector } from '@angular/core';
import { InputNumberComponent } from './input-number.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'input[number]',
  selector: 'formio-input-number',
  title: 'input[number]',
  group: 'custom',
  icon: 'hashtag',
  editForm: editForm,
  schema: {
    type: 'input[number]',
    varcontrol: 'input[number]',
    idvarcontrol: 4,
    key: 'NUMBER1',
    varname: 'NUMBER1',
    varbind: '',
    label: 'Number',
    varlabel: 'Number',
    input: true,
  }
};

export function registerInputNumberComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, InputNumberComponent, injector);
}