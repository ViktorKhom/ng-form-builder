import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FastFindComponent } from './fast-find.component';

describe('FastFindComponent', () => {
  let component: FastFindComponent;
  let fixture: ComponentFixture<FastFindComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FastFindComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FastFindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
