import { Injector } from '@angular/core';
import { LabelComponent } from './label.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'label',
  selector: 'formio-label',
  title: 'label',
  group: 'basic',
  icon: 'tag',
  editForm: editForm,
  schema: {
    type: 'label',
    varcontrol: 'label',
    idvarcontrol: 12,
    key: 'LABEL1',
    varname: 'LABEL1',
    varbind: '',
    label: 'LABEL1',
    varlabel: 'LABEL1',
    input: false
  }
};

export function registerLabelComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, LabelComponent, injector);
}