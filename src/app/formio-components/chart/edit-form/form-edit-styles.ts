const editFormStyles = [
    {
        type: 'panel',
        title: 'Стили',
        theme: 'default',
        components: [
            {
                weight: 0,
                type: 'select',
                input: true,
                key: 'reflection',
                label: 'reflection',
                placeholder: '',
                tooltip: 'Устанавливает стили видимости элемента',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'object-display (Элемент отображается как видимый)',
                          value: 'object-display'
                      },
                      {
                        label: 'object-display-hover (При наведении мыши на объект, дочерние и соседние элементы с установленным значением object-nodisplay отображаются как видимые)',
                        value: 'object-display-hover'
                      },
                      {
                        label: 'object-invisible (Элемент становится невидимым, т.е. полностью прозрачным, поскольку он продолжает участвовать в форматировании страницы)',
                        value: 'object-invisible'
                      },
                      {
                        label: 'object-nodisplay (Элемент становится невидимым, из документа удаляется и не участвует в форматировании страницы)',
                        value: 'object-nodisplay'
                      },
                      {
                        label: 'object-visible (Элемент отображается как видимый)',
                        value: 'object-visible'
                      },
                      {
                        label: 'object-visible-hover (При наведении мыши на объект, дочерние и соседние элементы с установленным значением object-invisible отображаются как видимые)',
                        value: 'object-visible-hover'
                      }
                  ]
                },
              }
        ]
    },
    {
        type: 'panel',
        title: 'Отступы и выравнивание',
        theme: 'default',
        components: [
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'margin',
                label: 'margin',
                placeholder: '',
                tooltip: 'Устанавливает величину отступа от каждого края элемента. Отступом является пространство от границы текущего элемента до внутренней границы его родительского элемента.',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-margin-bottom-em (отступ от нижнего края элемента em)',
                          value: 'art-margin-bottom-em'
                      },
                      {
                        label: 'art-margin-bottom-lg (отступ от нижнего края элемента lg)',
                        value: 'art-margin-bottom-lg'
                      },
                      {
                        label: 'art-margin-bottom-sm (отступ от нижнего края элемента sm)',
                        value: 'art-margin-bottom-sm'
                      },
                      {
                        label: 'art-margin-bottom-tm (отступ от нижнего края элемента tm)',
                        value: 'art-margin-bottom-tm'
                      },
                      {
                        label: 'art-margin-down-1 (сдвиг элемента вниз на 1px)',
                        value: 'art-margin-down-1'
                      },
                      {
                        label: 'art-margin-down-5 (сдвиг элемента вниз на 5px)',
                        value: 'art-margin-down-5'
                      },
                      {
                        label: 'art-margin-input-container (отступ для input c floating label без заголовка)',
                        value: 'art-margin-input-container'
                      },
                      {
                        label: 'art-margin-left-em (отступ от левого края элемента em)',
                        value: 'art-margin-left-em'
                      },
                      {
                        label: 'art-margin-left-lg (отступ от левого края элемента lg)',
                        value: 'art-margin-left-lg'
                      },
                      {
                        label: 'art-margin-left-right-em (отступ от правого и левого края элемента em)',
                        value: 'art-margin-left-right-em'
                      },
                      {
                        label: 'art-margin-left-right-lg (отступ от правого и левого края элемента lg)',
                        value: 'art-margin-left-right-lg'
                      },
                      {
                        label: 'art-margin-left-right-sm (отступ от правого и левого края элемента sm)',
                        value: 'art-margin-left-right-sm'
                      },
                      {
                        label: 'art-margin-left-sm (отступ от левого края элемента sm)',
                        value: 'art-margin-left-sm'
                      },
                      {
                        label: 'art-margin-off (Нулевой отступ)',
                        value: 'art-margin-off'
                      },
                      {
                        label: 'art-margin-right-em (отступ от правого края элемента em)',
                        value: 'art-margin-right-em'
                      },
                      {
                        label: 'art-margin-right-lg (отступ от правого края элемента lg)',
                        value: 'art-margin-right-lg'
                      },
                      {
                        label: 'art-margin-right-sm (отступ от правого края элемента sm)',
                        value: 'art-margin-right-sm'
                      },
                      {
                        label: 'art-margin-top-bottom-sm (отступ от верхнего и нижнего края элемента sm)',
                        value: 'art-margin-top-bottom-sm'
                      },
                      {
                        label: 'art-margin-top-em (отступ от верхнего края элемента em)',
                        value: 'art-margin-top-em'
                      },
                      {
                        label: 'art-margin-top-lg (отступ от верхнего края элемента lg)',
                        value: 'art-margin-top-lg'
                      },
                      {
                        label: 'art-margin-top-sm (отступ от верхнего края элемента sm)',
                        value: 'art-margin-top-sm'
                      },
                      {
                        label: 'art-margin-up-1 (сдвиг элемента вверх на 1px)',
                        value: 'art-margin-up-1'
                      },
                      {
                        label: 'art-margin-up-5 (сдвиг элемента вверх на 5px)',
                        value: 'art-margin-up-5'
                      },
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'padding',
                label: 'padding',
                placeholder: '',
                tooltip: 'Устанавливает значение полей вокруг содержимого  элемента. Полем называется расстояние от внутреннего края рамки элемента до содержимого элемента',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-padding-bottom-sm (Отступ от нижней границы - 20px)',
                          value: 'art-padding-bottom-sm'
                      },
                      {
                        label: 'art-padding-em (Отступ от верхней и нижней границы - 10px, отступ от правой и левой границы - 10px)',
                        value: 'art-padding-em'
                      },
                      {
                        label: 'art-padding-left-em (Отступ от левой границы - 10px)',
                        value: 'art-padding-left-em'
                      },
                      {
                        label: 'art-padding-left-right-em (Отступ от правой и левой границы - 10px)',
                        value: 'art-padding-left-right-em'
                      },
                      {
                        label: 'art-padding-left-right-lg (Отступ от правой и левой границы - 30px)',
                        value: 'art-padding-left-right-lg'
                      },
                      {
                        label: 'art-padding-left-right-sm (Отступ от правой и левой границы - 20px)',
                        value: 'art-padding-left-right-sm'
                      },
                      {
                        label: 'art-padding-left-right-tm (Отступ от правой и левой границы - 5px)',
                        value: 'art-padding-left-right-tm'
                      },
                      {
                        label: 'art-padding-left-sm (Отступ от левой границы - 20px)',
                        value: 'art-padding-left-sm'
                      },
                      {
                        label: 'art-padding-left-tm (Отступ от левой границы - 5px)',
                        value: 'art-padding-left-tm'
                      },
                      {
                        label: 'art-padding-off (Нулевой отступ)',
                        value: 'art-padding-off'
                      },
                      {
                        label: 'art-padding-right-em (Отступ от правой границы - 10px)',
                        value: 'art-padding-right-em'
                      },
                      {
                        label: 'art-padding-right-lg (Отступ от правой границы - 30px)',
                        value: 'art-padding-right-lg'
                      },
                      {
                        label: 'art-padding-right-sm (Отступ от правой границы - 20px)',
                        value: 'art-padding-right-sm'
                      },
                      {
                        label: 'art-padding-right-tm (Отступ от правой границы - 5px)',
                        value: 'art-padding-right-tm'
                      },
                      {
                        label: 'art-padding-sm (Отступ от верхней и нижней границы - 10px, отступ от правой и левой границы - 20px)',
                        value: 'art-padding-sm'
                      },
                      {
                        label: 'art-padding-tm (Отступ от верхней и нижней границы - 5px, отступ от правой и левой границы - 5px)',
                        value: 'art-padding-tm'
                      },
                      {
                        label: 'art-padding-top-em (Отступ от верхней границы - 10px)',
                        value: 'art-padding-top-em'
                      },
                      {
                        label: 'art-padding-top-mm (Отступ от верхней границы - 15px)',
                        value: 'art-padding-top-mm'
                      },
                      {
                        label: 'art-padding-top-sm (Отступ от верхней границы  - 20px)',
                        value: 'art-padding-top-sm'
                      },
                      {
                        label: 'art-padding-top-tm (Отступ от верхней границы - 5px)',
                        value: 'art-padding-top-tm'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'space-off',
                label: 'space-off',
                placeholder: '',
                tooltip: 'Удаление отступов',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-bottom-off (Убираем отступы внизу)',
                          value: 'art-bottom-off'
                      },
                      {
                        label: 'art-fieldset-space-off (все филдсеты на одном уровне)',
                        value: 'art-fieldset-space-off'
                      },
                      {
                        label: 'art-margin-bottom-off (margin-bottom=0 у всех элементов внутри тега с данным классом)',
                        value: 'art-margin-bottom-off'
                      },
                      {
                        label: 'art-rating (отступы для рейтинга)',
                        value: 'art-rating'
                      },
                      {
                        label: 'art-section-5 (отступы в section 5px)',
                        value: 'art-section-5'
                      },
                      {
                        label: 'art-section-space-off',
                        value: 'art-section-space-off'
                      },
                      {
                        label: 'art-space-off (margin=0, padding=0 у всех элементов внутри тега с данным классом)',
                        value: 'art-space-off'
                      },
                      {
                        label: 'art-space-off-1 (margin=0, padding=0 у всех дочерних элементов (1 уровня) элемента с данным классом)',
                        value: 'art-space-off-1'
                      },
                      {
                        label: 'art-space-off-2 (margin=0, padding=0 у всех дочерних элементов (2 уровня) элемента с данным классом)',
                        value: 'art-space-off-2'
                      },
                      {
                        label: 'art-space-off-3 (margin=0, padding=0 у всех дочерних элементов (3 уровня) элемента с данным классом)',
                        value: 'art-space-off-3'
                      },
                      {
                        label: 'art-top-bottom-off (Убираем отступы вверху и внизу)',
                        value: 'art-top-bottom-off'
                      },
                      {
                        label: 'art-top-off (Убираем все отступы вверху)',
                        value: 'art-top-off'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'div-position',
                label: 'div-position',
                placeholder: '',
                tooltip: 'Позиционирование слоя',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-position-above (Расположение слоя над остальным элементами родительского слоя)',
                          value: 'art-position-above'
                      },
                      {
                        label: 'art-position-absolute (Элемент абсолютно позиционирован, при этом другие элементы отображаются на веб-странице словно абсолютно позиционированного элемента и нет. Положение элемента задается свойствами left, top, right и bottom, также на положение влияет значение свойства position родительского элемента)',
                        value: 'art-position-absolute'
                      },
                      {
                        label: 'art-position-fixed (Элемент привязывается к указанной свойствами left, top, right и bottom точке на экране и не меняет своего положения при прокрутке веб-страницы)',
                        value: 'art-position-fixed'
                      },
                      {
                        label: 'art-position-relative (Положение элемента устанавливается относительно его исходного места. Добавление свойств left, top, right и bottom изменяет позицию элемента и сдвигает его в ту или иную сторону от первоначального расположения)',
                        value: 'art-position-relative'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'container',
                label: 'container',
                placeholder: '',
                tooltip: 'Контейнер, применяется для центровки контента',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-container (Контейнер фиксированной ширины - ширина области просмотра больше 768px - контейнер 746px - ширина области просмотра больше 992px - контейнер 966px - ширина области просмотра больше 1200px - контейнер 1166px)',
                          value: 'art-container'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'div-index',
                label: 'div-index',
                placeholder: '',
                tooltip: 'Размещение элемента по z-оси. Это свойство работает только для элементов, у которых значение position задано как absolute, fixed или relative. Чем больше значение, тем выше находится элемент по сравнению с теми элементами, у которых оно меньше.',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'div-index-0 (index=0)',
                          value: 'div-index-0'
                      },
                      {
                        label: 'div-index-10 (index=10)',
                        value: 'div-index-10'
                      },
                      {
                        label: 'div-index-100 (div-index-100)',
                        value: 'div-index-100'
                      },
                      {
                        label: 'div-index-1000 (index=1000)',
                        value: 'div-index-1000'
                      },
                      {
                        label: 'div-index-50 (index=50)',
                        value: 'div-index-50'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'div-up',
                label: 'div-up',
                placeholder: '',
                tooltip: 'Сдвиг элемента вверх',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'div-up-10 (Сдвиг на 10px вверх)',
                          value: 'div-up-10'
                      },
                      {
                        label: 'div-up-150 (Сдвиг на 150px вверх)',
                        value: 'div-up-150'
                      },
                      {
                        label: 'div-up-50 (Сдвиг на 50px вверх)',
                        value: 'div-up-50'
                      }
                  ]
                },
            }
        ]
    },
    {
        type: 'panel',
        title: 'Стили элементов',
        theme: 'default',
        components: [
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'background',
                label: 'background',
                placeholder: '',
                tooltip: 'Устанавливает стиль фона',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-bg-color-input-grey (Не использовать без согласования)',
                          value: 'art-bg-color-input-grey'
                      },
                      {
                        label: 'art-bg-color-input-yellow (Не использовать без согласования)',
                        value: 'art-bg-color-input-yellow'
                      },
                      {
                        label: 'art-bg-color-row-grid (Цвет фона строки для грида)',
                        value: 'art-bg-color-row-grid'
                      },
                      {
                        label: 'art-bg-ext-1 (Дополнительный цвет фона 1)',
                        value: 'art-bg-ext-1'
                      },
                      {
                        label: 'art-bg-ext-2 (Дополнительный цвет фона 2)',
                        value: 'art-bg-ext-2'
                      },
                      {
                        label: 'art-bg-ext-3 (Дополнительный цвет фона 3)',
                        value: 'art-bg-ext-3'
                      },
                      {
                        label: 'art-bg-header (Для header)',
                        value: 'art-bg-header'
                      },
                      {
                        label: 'art-bg-main-1 (Цвет фона для выделения слоя 1)',
                        value: 'art-bg-main-1'
                      },
                      {
                        label: 'art-bg-main-2 (Цвет фона для выделения слоя 2)',
                        value: 'art-bg-main-2'
                      },
                      {
                        label: 'art-bg-message (Цвет фона для message)',
                        value: 'art-bg-message'
                      },
                      {
                        label: 'art-bg-message-source (Цвет фона сообщения источника)',
                        value: 'art-bg-message-source'
                      },
                      {
                        label: 'art-bg-message-target (Цвет фона сообщения приемника)',
                        value: 'art-bg-message-target'
                      },
                      {
                        label: 'art-bg-primary-1 (Основной цвет фона 1)',
                        value: 'art-bg-primary-1'
                      },
                      {
                        label: 'art-bg-primary-2 (Основной цвет фона 2)',
                        value: 'art-bg-primary-2'
                      },
                      {
                        label: 'art-bg-row (Цвет фона при наведении мыши на объект. Используется, например,  для выделения строки списка)',
                        value: 'art-bg-row'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'panel',
                label: 'panel',
                placeholder: '',
                tooltip: 'Устанавливает стиль блока элементов',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-fit-size (Автоматическая высота по высоте окна отступ справа и слева 10px, с полосой прокрутки)',
                          value: 'art-fit-size'
                      },
                      {
                        label: 'art-panel-10 (Высота блока 10% от высоты экрана, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-10'
                      },
                      {
                        label: 'art-panel-20 (Высота блока 20% от высоты экрана, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-20'
                      },
                      {
                        label: 'art-panel-30 (Высота блока 30% от высоты экрана, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-30'
                      },
                      {
                        label: 'art-panel-40 (Высота блока 40% от высоты экрана, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-40'
                      },
                      {
                        label: 'art-panel-50 (Высота блока 50% от высоты экрана, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-50'
                      },
                      {
                        label: 'art-panel-60 (Высота блока 60% от высоты экрана, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-60'
                      },
                      {
                        label: 'art-panel-70 (Высота блока 70% от высоты экрана, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-70'
                      },
                      {
                        label: 'art-panel-80 (Высота блока 80% от высоты экрана, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-80'
                      },
                      {
                        label: 'art-panel-90 (Высота блока 90% от высоты экрана, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-90'
                      },
                      {
                        label: 'art-panel-xg (Высота блока 875 px, отступ справа и слева 10px, с полосой прокрутки)',
                        value: 'art-panel-xg'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'text-color',
                label: 'text-color ',
                placeholder: '',
                tooltip: 'Устанавливает цвет текста',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-txt-disable (Цвет текста для disable-элементов)',
                          value: 'art-txt-disable'
                      },
                      {
                        label: 'art-txt-ext-1 (Дополнительный цвет 1)',
                        value: 'art-txt-ext-1'
                      },
                      {
                        label: 'art-txt-ext-2 (Дополнительный цвет 2)',
                        value: 'art-txt-ext-2'
                      },
                      {
                        label: 'art-txt-ext-3 (Дополнительный цвет 3)',
                        value: 'art-txt-ext-3'
                      },
                      {
                        label: 'art-txt-input-1 (Цвет шрифта для элемента ввода 1)',
                        value: 'art-txt-input-1'
                      },
                      {
                        label: 'art-txt-input-2 (Цвет шрифта для элемента ввода 2)',
                        value: 'art-txt-input-2'
                      },
                      {
                        label: 'art-txt-primary-1 (Основной цвет 1)',
                        value: 'art-txt-primary-1'
                      },
                      {
                        label: 'art-txt-primary-2 (Основной цвет 2)',
                        value: 'art-txt-primary-2'
                      },
                      {
                        label: 'art-txt-primary-3 (Основной цвет 3)',
                        value: 'art-txt-primary-3'
                      },
                      {
                        label: 'art-txt-primary-4 (Основной цвет 4)',
                        value: 'art-txt-primary-4'
                      },
                      {
                        label: 'art-txt-warning (Цвет текста для warning)',
                        value: 'art-txt-warning'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'align',
                label: 'align',
                placeholder: '',
                tooltip: 'Выравнивание содержимого элемента',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-align-justify (Выравнивание по ширине, что означает одновременное выравнивание по левому и правому краю (добавляются пробелы между словами).)',
                          value: 'art-align-justify'
                      },
                      {
                        label: 'art-align_center (Выравнивание по центру)',
                        value: 'art-align_center'
                      },
                      {
                        label: 'art-align_left (Выравнивание по левому краю)',
                        value: 'art-align_left'
                      },
                      {
                        label: 'art-align_right (Выравнивание по правому краю)',
                        value: 'art-align_right'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'font-size',
                label: 'font-size',
                placeholder: '',
                tooltip: 'Устанавливает размер шрифта',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                        label: 'art-font-lg (размер шрифта 160% (100% - размер шрифта родительского элемента))',
                        value: 'art-font-lg'
                      },
                      {
                        label: 'art-font-md (размер шрифта 130% (100% - размер шрифта родительского элемента))',
                        value: 'art-font-md'
                      },
                      {
                        label: 'art-font-sm (размер шрифта 95% (100% - размер шрифта родительского элемента))',
                        value: 'art-font-sm'
                      },
                      {
                        label: 'art-font-x2l (размер шрифта 230% (100% - размер шрифта родительского элемента))',
                        value: 'art-font-x2l'
                      },
                      {
                        label: 'art-font-x3l (размер шрифта 260% (100% - размер шрифта родительского элемента))',
                        value: 'art-font-x3l'
                      },
                      {
                        label: 'art-font-x4l (размер шрифта 300% (100% - размер шрифта родительского элемента))',
                        value: 'art-font-x4l'
                      },
                      {
                        label: 'art-font-xl (размер шрифта 200% (100% - размер шрифта родительского элемента))',
                        value: 'art-font-xl'
                      },
                      {
                        label: 'art-font-xs (размер шрифта 85% (100% - размер шрифта родительского элемента))',
                        value: 'art-font-xs'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'text-weight',
                label: 'text-weight',
                placeholder: '',
                tooltip: 'Устанавливает насыщенность шрифта',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-txt-bold (Жирный текст)',
                          value: 'art-txt-bold'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'text-line',
                label: 'text-line',
                placeholder: '',
                tooltip: 'Устанавливает оформление текста',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-txt-italic (Курсивное начертание)',
                          value: 'art-txt-italic'
                      },
                      {
                        label: 'art-txt-line (Перечеркнутый текст)',
                        value: 'art-txt-line'
                      },
                      {
                        label: 'art-txt-underline (Подчеркнутый текст)',
                        value: 'art-txt-underline'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'text-transform',
                label: 'text-transform',
                placeholder: '',
                tooltip: 'Преобразование текста в заглавные или прописные символы',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-txt-capitalize (Первый символ каждого слова заглавный)',
                          value: 'art-txt-capitalize'
                      },
                      {
                        label: 'art-txt-lowercase (Нижний регистр)',
                        value: 'art-txt-line'
                      },
                      {
                        label: 'art-txt-uppercase (Верхний регистр)',
                        value: 'art-txt-uppercase'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'text-space',
                label: 'text-space',
                placeholder: '',
                tooltip: 'Устанавливает отображение пробелов между словами',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-space-no-wrap (Пробелы не учитываются, переносы строк в коде HTML игнорируются, весь текст отображается одной строкой; вместе с тем, добавление тега <br> переносит текст на новую строку)',
                          value: 'art-space-no-wrap'
                      },
                      {
                        label: 'art-txt-lowercase (Нижний регистр)',
                        value: 'art-txt-line'
                      },
                      {
                        label: 'art-txt-uppercase (Верхний регистр)',
                        value: 'art-txt-uppercase'
                      },
                      {
                        label: 'art-space-normal (Текст в окне браузера выводится как обычно, переносы строк устанавливаются автоматически)',
                        value: 'art-space-normal'
                      },
                      {
                        label: 'art-space-pre-wrap (В тексте сохраняются все пробелы и переносы, однако если строка по ширине не помещается в заданную область, то текст автоматически будет перенесен на следующую строку)',
                        value: 'art-space-pre-wrap'
                      },
                      {
                        label: 'art-space-word-break (Перенос слов на другую строку, если слово не помещается в заданную область. (без применения правил переноса слов))',
                        value: 'art-space-word-break'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'text-letter-spacing ',
                label: 'text-letter-spacing ',
                placeholder: '',
                tooltip: 'Определяет стиль заголовка',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'letter-spacing-em (Интервал между символами 0.05em)',
                          value: 'art-letter-spacing-em'
                      },
                      {
                        label: 'letter-spacing-lg (Интервал между символами 0.2em)',
                        value: 'art-letter-spacing-lg'
                      },
                      {
                        label: 'letter-spacing-md (Интервал между символами 0.15em)',
                        value: 'art-letter-spacing-md'
                      },
                      {
                        label: 'art-space-normal (Текст в окне браузера выводится как обычно, переносы строк устанавливаются автоматически)',
                        value: 'art-space-normal'
                      },
                      {
                        label: 'letter-spacing-sm (Интервал между символами 0.1em)',
                        value: 'art-letter-spacing-sm'
                      }
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'text-header',
                label: 'text-header',
                placeholder: '',
                tooltip: 'Определяет стиль заголовка',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-h1 (Заголовок стиль-1)',
                          value: 'art-h1'
                      },
                      {
                        label: 'art-h2 (Заголовок стиль-2)',
                        value: 'art-h2'
                      },
                      {
                        label: 'art-h3 (Заголовок стиль-3)',
                        value: 'art-h3'
                      },
                      {
                        label: 'art-h4 (Заголовок стиль-4)',
                        value: 'art-h4'
                      },
                      {
                        label: 'art-h5 (Заголовок стиль-5)',
                        value: 'art-h5'
                      }
                  ]
                },
            }
        ]
    },
    {
        type: 'panel',
        title: 'Стили рамки',
        theme: 'default',
        components: [
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'border',
                label: 'border',
                placeholder: '',
                tooltip: 'Устанавливает толщину, стиль и цвет границы вокруг элемента.',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-border-off (Отключить рамки)',
                          value: 'art-border-off'
                      },
                      {
                        label: 'art-borders-add (Дополнительный стиль рамки.рамка: все границы элемента, толщина - 1px цвет: серый #CCCCCC вид: solid)',
                        value: 'art-borders-add'
                      },
                      {
                        label: 'art-borders-add-bottom (Дополнительный стиль рамки. рамка: нижняя граница элемента, толщина - 1px цвет: серый #CCCCCC вид: solid)',
                        value: 'art-borders-add-bottom'
                      },
                      {
                        label: 'art-borders-add-right-bottom (Дополнительный стиль рамки. рамка: правая и нижняя граница элемента, толщина - 1px цвет: серый #CCCCCC вид: solid)',
                        value: 'art-borders-add-right-bottom'
                      },
                      {
                        label: 'art-borders-card (Стиль рамки для карточки)',
                        value: 'art-borders-card'
                      },
                      {
                        label: 'art-borders-primary (Основной стиль рамки. рамка: все границы элемента, толщина - 1px цвет: оранжевый #FF9900 вид: solid)',
                        value: 'art-borders-primary'
                      },
                      {
                        label: 'art-borders-primary-shadow (Oсновной стиль рамки с тенью. рамка: все границы элемента, толщина - 1px цвет: оранжевый #FF9900 вид: solid  тень: по правому и нижнему краю, цвет оранжевый #FF9900)',
                        value: 'art-borders-primary-shadow'
                      },
                      {
                        label: 'art-borders-shadow (Стиль рамки с тенью)',
                        value: 'art-borders-shadow'
                      },
                  ]
                },
            },
            {
                weight: 20,
                type: 'select',
                input: true,
                key: 'shadow',
                label: 'shadow',
                placeholder: '',
                tooltip: 'Устанавливает стиль тени вокруг элементов',
                dataSrc: 'values',
                data: {
                  values: [
                      {
                          label: 'art-shadow (Тень вокруг элемента цвет: серый #CCCCCC)',
                          value: 'art-shadow'
                      }
                  ]
                },
            },
        ]
    }
  ];
  
  
  export default editFormStyles;