import { Injector } from '@angular/core';
import { InputDateTimeComponent } from './input-date-time.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'inputDateTime',
  selector: 'input-date-time',
  title: 'input[datetime]',
  group: 'special',
  icon: 'calendar-plus-o',
  editForm: editForm,
  schema: {
    type: 'input[datetime]',
    varcontrol: 'input[datetime]',
    idvarcontrol: 24,
    key: 'DATETIME1',
    varname: 'DATETIME1',
    varbind: '',
    label: 'Datetime',
    varlabel: 'Datetime',
    input: true,
  }
};

export function registerInputDateTimeComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, InputDateTimeComponent, injector);
}