import { Injector } from '@angular/core';
import { UiCarouselComponent } from './ui-carousel.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'uiCarousel',
  selector: 'formio-ui-carousel',
  title: 'ui-carousel',
  group: 'special',
  icon: 'picture-o',
  editForm: editForm,
  schema: {
    type: 'ui-carousel',
    varcontrol: 'ui-carousel',
    idvarcontrol: 39,
    key: 'UI-CAROUSEL1',
    varname: 'UI-CAROUSEL1',
    varbind: '',
    label: 'Ui-carousel',
    varlabel: 'Ui-carousel',
  }
};

export function registerUiFormioComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, UiCarouselComponent, injector);
}