import { Injector } from '@angular/core';
import { FormComponent } from './form.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'form',
  selector: 'formio-form',
  title: 'Form',
  group: 'special',
  icon: 'wpforms',
  editForm: editForm,
  schema: {
    varcontrol: 'form',
    varname: 'FORM1',
    varlabel: 'FORM1',
    clearOnHide: true,
    input: true,
    tableView: true,
    key: 'formField',
    idvarcontrol: 30,
    templatevar:"",
    src: '',
    reference: true,
    form: '',
    path: '',
    label: '',
    protected: false,
    unique: false,
    persistent: true
  }
};

export function registerFormComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, FormComponent, injector);
}