import { Components } from 'formiojs';
const NestedComponent = (Components as any).components.nested;
import * as _tooltip from "tooltip.js";
import  { editForm }  from './edit-form/edit-form.index';

export default class ListHContainerComponent extends NestedComponent {
  static schema(...extend) {
    return NestedComponent.schema({
      type: 'list-h-container',
      label:'list-h-container',
      varcontrol: "list-h-container",
      idvarcontrol: 69,
      key: "LISTCONTAINER",
      varname: "LISTCONTAINER",
      varlabel: 'container-h-list',
      varbind: '',
      components: [],	
    }, ...extend);
  }

  static get builderInfo() {
    return {
      title: 'list-h-container',
      group: 'layout',
      icon: 'th-large',
      weight: 70,
      schema: ListHContainerComponent.schema()
    };
  }

  get defaultSchema() {
    return ListHContainerComponent.schema();
  }

  get className() {
    return `form-group ${super.className} list-h-container`;
  }

  get templateName() {
    return 'fieldset';
  }

  attach(element) {
    const refs = {'showTitle': ''};
    this.loadRefs(element, refs);
    this.tooltip = new _tooltip.default(element, {
      trigger: 'hover click',
      placement: 'top',
      html: true,
      title: this.t(this.component.key)
    });
    return super.attach(element);
  }


  constructor(component, options, data) {
    super(component, options, data);
    this.noField = true;
    this.component = component;
  }
}

ListHContainerComponent.editForm = editForm;