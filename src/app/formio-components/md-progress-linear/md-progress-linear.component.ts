import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormioCustomComponent } from 'angular-formio';

@Component({
  selector: 'app-md-progress-linear',
  templateUrl: './md-progress-linear.component.html',
  styleUrls: ['./md-progress-linear.component.css']
})
export class MdProgressLinearComponent implements FormioCustomComponent<any> {

  @Input()
  value: string;

  @Output()
  valueChange = new EventEmitter<string>();

  @Input()
  disabled: boolean;

}
