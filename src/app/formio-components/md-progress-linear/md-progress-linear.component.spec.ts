import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdProgressLinearComponent } from './md-progress-linear.component';

describe('MdProgressLinearComponent', () => {
  let component: MdProgressLinearComponent;
  let fixture: ComponentFixture<MdProgressLinearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdProgressLinearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdProgressLinearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
