import { Injector } from '@angular/core';
import { ImageComponent } from './image.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'image',
  selector: 'formio-image',
  title: 'image',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'image',
    varcontrol: 'image',
    idvarcontrol: 29,
    key: 'IMAGE1',
    varname: 'IMAGE1',
    varbind: '',
    label: 'Image',
    varlabel: 'Image'
  }
};

export function registerImageComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, ImageComponent, injector);
}