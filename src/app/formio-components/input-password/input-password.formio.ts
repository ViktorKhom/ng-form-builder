import { Injector } from '@angular/core';
import { InputPasswordComponent } from './input-password.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'inputPassword',
  selector: 'input-password',
  title: 'input[password]',
  group: 'special',
  icon: 'asterisk',
  editForm: editForm,
  schema: {
    type: 'input[password]',
    varcontrol: 'input[password]',
    idvarcontrol: 20,
    key: 'PASSWORD1',
    varname: 'PASSWORD1',
    varbind: '',
    label: 'Password',
    varlabel: 'Password',
    input: true,
  }
};

export function registerInputPasswordComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, InputPasswordComponent, injector);
}