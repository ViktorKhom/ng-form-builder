import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormioCustomComponent } from 'angular-formio';

@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.css']
})
export class AutoCompleteComponent implements FormioCustomComponent<any> {

  @Input()
  value: string;

  @Output()
  valueChange = new EventEmitter<string>();

  @Input()
  disabled: boolean;

}
