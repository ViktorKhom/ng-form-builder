import { Injector } from '@angular/core';
import { AutoCompleteComponent } from './auto-complete.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'autoComplete',
  selector: 'formio-auto-complete',
  title: 'autocomplete',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'autocomplete',
    varcontrol: 'autocomplete',
    idvarcontrol: 47,
    key: 'AUTOCOMPLETE1',
    varname: 'AUTOCOMPLETE1',
    varbind: '',
    label: 'Autocomplete',
    varlabel: 'Autocomplete',
  }
};

export function registerAutoCompleteComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, AutoCompleteComponent, injector);
}