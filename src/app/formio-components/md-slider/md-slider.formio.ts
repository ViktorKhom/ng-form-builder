import { Injector } from '@angular/core';
import { MdSliderComponent } from './md-slider.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'mdSlider',
  selector: 'formio-md-slider',
  title: 'md-slider',
  group: 'special',
  icon: 'sliders',
  editForm: editForm,
  schema: {
    type: 'md-slider',
    varcontrol: 'md-slider',
    idvarcontrol: 38,
    key: 'MD-SLIDER1',
    varname: 'MD-SLIDER1',
    varbind: '',
    label: 'Md-slider',
    varlabel: 'Md-slider',
  }
};

export function registerMdSliderComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, MdSliderComponent, injector);
}