import { Injector } from '@angular/core';
import { UiModelPanelComponent } from './ui-model-panel.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'uiModelPanel',
  selector: 'formio-ui-model-panel',
  title: 'ui-model-panel',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'ui-model-panel',
    varcontrol: 'ui-model-panel',
    idvarcontrol: 22,
    key: 'UI-MODEL-PANEL1',
    varname: 'UI-MODEL-PANEL1',
    varbind: '',
    label: 'Ui-model-panel',
    varlabel: 'Ui-model-panel',
  }
};

export function registerUiModelPanelComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, UiModelPanelComponent, injector);
}