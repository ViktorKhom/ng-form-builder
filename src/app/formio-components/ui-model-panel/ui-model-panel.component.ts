import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormioCustomComponent } from 'angular-formio';

@Component({
  selector: 'app-ui-model-panel',
  templateUrl: './ui-model-panel.component.html',
  styleUrls: ['./ui-model-panel.component.css']
})
export class UiModelPanelComponent implements FormioCustomComponent<any> {

  @Input()
  value: string;

  @Output()
  valueChange = new EventEmitter<string>();

  @Input()
  disabled: boolean;

}
