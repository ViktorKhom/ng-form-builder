import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiModelPanelComponent } from './ui-model-panel.component';

describe('UiModelPanelComponent', () => {
  let component: UiModelPanelComponent;
  let fixture: ComponentFixture<UiModelPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiModelPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiModelPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
